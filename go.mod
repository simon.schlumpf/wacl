module gitlab.com/simon.schlumpf/wacl

go 1.16

require (
	github.com/go-chi/chi v1.5.4
	github.com/rs/zerolog v1.21.0
)
