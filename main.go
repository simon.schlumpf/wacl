package wacl

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"reflect"
	"regexp"
	"strings"

	"github.com/go-chi/chi/middleware"
	"github.com/rs/zerolog"
)

/*
func main() {
	var w WACL
	n, err := w.LoadConfigFromFile("../wacl.json")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Loaded", n, "Rule(s)")
	w.SetGlobalVariable("name", "Test")
	w.PrintConfig()
}
*/

// WACL is the main Web ACL struct
type WACL struct {
	Rules             map[string]waclRule    `json:"Rules"`
	OnNotFound        map[string]interface{} `json:"OnNotFound"`
	GlobalVariables   map[string]interface{} `json:",omitempty"`
	matchFunctions    map[string]func(matchName string, matchValue interface{}, r *http.Request, lV map[string]interface{}, log *zerolog.Logger) bool
	checkFunctions    map[string]func(checkName string, checkValue interface{}, r *http.Request, lV map[string]interface{}, log *zerolog.Logger) bool
	OnRequestFunction func(w http.ResponseWriter, r *http.Request, lV map[string]interface{}, log *zerolog.Logger) `json:",omitempty"`
	Log               *zerolog.Logger                                                                              `json:",omitempty"`
}

// LoadConfigFromFile loads a wacl config from a JSON file
func (wr *WACL) LoadConfigFromFile(file string, logger *zerolog.Logger) (n int, err error) {
	wr.Log = logger
	jsonin, err := ioutil.ReadFile(file)
	if err != nil {
		return 0, fmt.Errorf("")
	}
	err = json.Unmarshal([]byte(jsonin), wr)
	if err != nil {
		wr.Log.Fatal().Fields(map[string]interface{}{
			"type":  "Web ACL",
			"error": err.Error(),
		}).Msg("Error encoding JSON")
		os.Exit(1)
	}
	n = len(wr.Rules)
	wr.GlobalVariables = make(map[string]interface{})

	wr.matchFunctions = make(map[string]func(matchName string, matchValue interface{}, r *http.Request, lV map[string]interface{}, log *zerolog.Logger) bool)
	wr.checkFunctions = make(map[string]func(checkName string, checkValue interface{}, r *http.Request, lV map[string]interface{}, log *zerolog.Logger) bool)

	wr.initDefaultFunctions()

	wr.Log.Info().
		Fields(map[string]interface{}{
			"type": "Web ACL",
		}).
		Msg("Successfully loaded WebACL config")

	return
}

// AddMatchFunction adds - or overwrites - a function to match a HTTP request
func (wr *WACL) AddMatchFunction(name string, f func(matchName string, matchValue interface{}, r *http.Request, lV map[string]interface{}, log *zerolog.Logger) bool) {
	wr.matchFunctions[name] = f
}

// AddCheckFunction adds - or overwrites - a function to check a HTTP request
func (wr *WACL) AddCheckFunction(name string, f func(matchName string, matchValue interface{}, r *http.Request, lV map[string]interface{}, log *zerolog.Logger) bool) {
	wr.checkFunctions[name] = f
}

// SetGlobalVariable adds or overwrites a variable in the global wacl scope
func (wr *WACL) SetGlobalVariable(name string, value interface{}) {
	wr.GlobalVariables[name] = value
}

// GetGlobalVariable returns the value of a variable. exists is false when the variable does not exist
func (wr *WACL) GetGlobalVariable(name string) (exists bool, value interface{}) {
	value, exists = wr.GlobalVariables[name]
	return
}

// CheckRequest checks a request against the rules and acts on the given rules
// This function can be used as middleware
func (wr *WACL) CheckRequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		localVariables := make(map[string]interface{})
		var matchFound bool
		matchFound = false
		wr.OnRequestFunction(w, r, localVariables, wr.Log)
		reqID := middleware.GetReqID(r.Context())
	Rule:
		for ruleName, rule := range wr.Rules {
			// Match:
			wr.Log.Debug().
				Fields(map[string]interface{}{
					"type":                    "Web ACL",
					"req_id":                  reqID,
					"rule_name":               ruleName,
					"num_of_match_conditions": len(rule.Match),
				}).Msg("Entering Rule")
			if len(rule.Match) > 0 {
				for matchName, matchValue := range rule.Match {
					wr.Log.Debug().
						Fields(map[string]interface{}{
							"type":       "Web ACL",
							"req_id":     reqID,
							"rule_name":  ruleName,
							"match_name": matchName,
						}).Msg("Entering Match")
					if f, ok := wr.matchFunctions[matchName]; ok {
						if f(matchName, matchValue, r, localVariables, wr.Log) {
							wr.Log.Debug().
								Fields(map[string]interface{}{
									"type":       "Web ACL",
									"req_id":     reqID,
									"rule_name":  ruleName,
									"match_name": matchName,
									"verdict":    true,
								}).Msg("Evaluated Match")
							continue
						}
						wr.Log.Debug().
							Fields(map[string]interface{}{
								"type":       "Web ACL",
								"req_id":     reqID,
								"rule_name":  ruleName,
								"match_name": matchName,
								"verdict":    false,
							}).Msg("Evaluated Match, skipping rule")
						continue Rule
					} else {
						wr.Log.Info().
							Fields(map[string]interface{}{
								"type":       "Web ACL",
								"req_id":     reqID,
								"rule_name":  ruleName,
								"match_name": matchName,
							}).Msg("Match is not implemented, skipping rule")
						continue Rule
					}
				}
			}
			matchFound = true
			wr.Log.Debug().
				Fields(map[string]interface{}{
					"type":                    "Web ACL",
					"req_id":                  reqID,
					"rule_name":               ruleName,
					"num_of_check_conditions": len(rule.Check),
				}).Msg("All matches are fulfilled, proceeding with Checks")
			//Check:
			if len(rule.Check) > 0 {
				for checkName, checkValue := range rule.Check {
					wr.Log.Debug().
						Fields(map[string]interface{}{
							"type":       "Web ACL",
							"req_id":     reqID,
							"rule_name":  ruleName,
							"check_name": checkName,
						}).Msg("Entering Check")
					if f, ok := wr.checkFunctions[checkName]; ok {
						if f(checkName, checkValue, r, localVariables, wr.Log) {
							wr.Log.Debug().
								Fields(map[string]interface{}{
									"type":       "Web ACL",
									"req_id":     reqID,
									"rule_name":  ruleName,
									"check_name": checkName,
									"verdict":    true,
								}).Msg("Evaluated Check")
							continue
						} else {
							wr.Log.Info().
								Fields(map[string]interface{}{
									"type":       "Web ACL",
									"req_id":     reqID,
									"rule_name":  ruleName,
									"check_name": checkName,
									"verdict":    false,
								}).Msg("Evaluated Check: Failed, proceed with OnFail")
							/////////////////////
							// NO ON CHECK FAILED
							/////////////////////
							// Example: w.WriteHeader(rule.OnFail["StatusCode"])
							// Example: w.Write([]byte(rule.OnFail["ResponseBody"])
							// but it's map Interface, check types etc..

							w.WriteHeader(http.StatusForbidden)
							w.Write([]byte("Check Failed: " + checkName))
							return
						}
					} else {
						wr.Log.Info().
							Fields(map[string]interface{}{
								"type":       "Web ACL",
								"req_id":     reqID,
								"check_name": checkName,
								"rule_name":  ruleName,
							}).Msg("Check is not implemented")
						w.WriteHeader(http.StatusForbidden)
						w.Write([]byte("Check Failed: " + checkName))
						return
					}
				}
			} else {
				break Rule
			}
		}
		if matchFound {
			wr.Log.Debug().
				Fields(map[string]interface{}{
					"type":   "Web ACL",
					"req_id": reqID,
				}).Msg("All checks are fulfilled, proceed with OnSuccess")
			next.ServeHTTP(w, r)
			return
		}
		wr.Log.Info().
			Fields(map[string]interface{}{
				"type":   "Web ACL",
				"req_id": reqID,
				"uri":    r.URL.Path,
				"method": r.Method,
			}).Msg("No match, proceed with OnNotFound.")
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("400 - Bad Request"))

		//w.WriteHeader(wr.OnNotFound["StatusCode"].(int))
		//w.Write(wr.OnNotFound["StatusMessage"].([]byte))

		return
	})

}

// PrintConfig prints human readable (almost) config output to stdout
func (wr *WACL) PrintConfig() {
	for n, r := range wr.Rules {
		fmt.Printf("Name: %s\nDescription: %s\n", n, r.Description)
		fmt.Printf("Match:\n")
		for mn, m := range r.Match {
			fmt.Printf("\t%s:  %s\n", mn, m)
		}
		fmt.Printf("Check:\n")
		for cn, c := range r.Check {
			fmt.Printf("\t%s:  %s\n", cn, c)
		}
		fmt.Printf("OnFail:\n")
		for fn, f := range r.OnFail {
			fmt.Printf("\t%s:  %s\n", fn, f)
		}
		fmt.Printf("OnSuccess:\n")
		for sn, s := range r.OnSuccess {
			fmt.Printf("\t%s:  %s\n", sn, s)
		}
		fmt.Println("----------------------")
	}
	fmt.Printf("OnNotFound:\n")
	for on, o := range wr.OnNotFound {
		fmt.Printf("\t%s:  %s\n", on, o)
	}
	fmt.Println("----------------------")
	fmt.Printf("Global Variables:\n")
	for on, o := range wr.GlobalVariables {
		fmt.Printf("\t%s:  %s\n", on, o)
	}
}

type waclRule struct {
	Name        string
	Description string
	Match       map[string]interface{}
	Check       map[string]interface{}
	OnFail      map[string]interface{}
	OnSuccess   map[string]interface{}
}

func (wr *WACL) initDefaultFunctions() {
	wr.AddMatchFunction("Method", func(matchName string, matchValue interface{}, r *http.Request, lV map[string]interface{}, log *zerolog.Logger) bool {
		mvs := fmt.Sprintf("%v", matchValue)
		reqID := middleware.GetReqID(r.Context())
		if mvs == r.Method {
			wr.Log.Debug().
				Fields(map[string]interface{}{
					"type":    "Web ACL",
					"req_id":  reqID,
					"asked":   mvs,
					"given":   r.Method,
					"verdict": true,
				}).Msg("Comparing Method")
			return true
		}
		wr.Log.Debug().
			Fields(map[string]interface{}{
				"type":    "Web ACL",
				"req_id":  reqID,
				"asked":   mvs,
				"given":   r.Method,
				"verdict": false,
			}).Msg("Comparing Method")
		return false
	})

	wr.AddMatchFunction("URI", func(matchName string, matchValue interface{}, r *http.Request, lV map[string]interface{}, log *zerolog.Logger) bool {
		if reflect.TypeOf(matchValue).String() != "string" {
			fmt.Println("URI ist not a String, it's a", reflect.TypeOf(matchValue).String())
			return false
		}
		var Exp = regexp.MustCompile(fmt.Sprintf("%v", matchValue))
		match := Exp.FindStringSubmatch(r.RequestURI)
		if match == nil {
			return false
		}
		for i, name := range Exp.SubexpNames() {
			if i != 0 && name != "" {
				lV[name] = match[i]
			}
		}
		/*fmt.Printf("regexp: %v\n", matchValue)
		for i, name := range Exp.SubexpNames() {
			fmt.Printf("i: %v, name: %v\n", i, name)
		}
		//fmt.Println("r.RequestURI:", r.RequestURI)
		/*fmt.Printf("Local Variables:\n")
		for on, o := range lV {
			fmt.Printf("\t%s:  %s\n", on, o)
		}*/
		return true
	})

	wr.AddCheckFunction("VarComp", func(checkName string, checkValue interface{}, r *http.Request, lV map[string]interface{}, log *zerolog.Logger) bool {
		if reflect.TypeOf(checkValue).String() != "string" {
			fmt.Println("VarComp value ist not a String, it's a", reflect.TypeOf(checkValue).String())
			return false
		}
		checkValueString := fmt.Sprintf("%v", checkValue)

		for k, v := range wr.GlobalVariables {
			vs := fmt.Sprintf("%v", v)
			checkValueString = strings.ReplaceAll(checkValueString, "${"+k+"}", vs)
		}
		for k, v := range lV {
			vs := fmt.Sprintf("%v", v)
			checkValueString = strings.ReplaceAll(checkValueString, "${"+k+"}", vs)
		}
		CompFunc := "none"
		var args []string
		if strings.Contains(checkValueString, "==") {
			CompFunc = "Equal"
			args = strings.Split(checkValueString, "==")
		} else if strings.Contains(checkValueString, "!=") {
			CompFunc = "NotEqual"
			args = strings.Split(checkValueString, "!=")
		} else if strings.Contains(checkValueString, ">") {
			CompFunc = "Greater"
			args = strings.Split(checkValueString, ">")
		} else if strings.Contains(checkValueString, "<") {
			CompFunc = "Less"
			args = strings.Split(checkValueString, "<")
		} else {
			//Function not implemented
		}

		switch CompFunc {
		case "Equal":
			if args[0] == args[1] {
				return true
			}
			return false
		case "NotEqual":
			if args[0] != args[1] {
				return true
			}
			return false
		}
		return false
	})

	wr.AddCheckFunction("SessionExists", func(checkName string, checkValue interface{}, r *http.Request, lV map[string]interface{}, log *zerolog.Logger) bool {
		if val, ok := lV["Session.Exists"]; ok {
			if reflect.TypeOf(val).String() != "bool" {
				fmt.Println("checkValue ist not a bool, it's a", reflect.TypeOf(checkValue).String())
				return false
			}
			if fmt.Sprintf("%v", val) == "true" {
				return true
			}
		}
		return false
	})
}
